import React, { useCallback, useEffect } from 'react';

import "./App.scss";
import { getTasks } from './gantt/data.ts';
import { CustomComponent } from './gantt/CustomComponent.tsx';
import { useObservableState } from 'observable-hooks';
import {
    updateFirstEvent,
    dummyFilter$,
    tasks$,
    updateFilter,
    updateTasks, toggleSettings, dummySettings$
} from './gantt/state.ts';

function App() {
  const tasks = useObservableState(tasks$, []);
  const filterEnabled = useObservableState(dummyFilter$, false);
  const settingsEnabled = useObservableState(dummySettings$, false);

  useEffect(() => {
    updateTasks(getTasks(20));
  }, []);

  const handleToggleFilter = useCallback(
      () => {
          updateFilter(!filterEnabled);
      },
      [filterEnabled]
  );

  const handleChangeFirstEvent = useCallback(() => {
    updateFirstEvent();
  }, [])

    const handleToggleSettings = useCallback(() => {
        toggleSettings();
    }, [])

  return (
    <>
      <div>
        <button onClick={handleToggleFilter}>Toggle filter ({ filterEnabled ? 'Enabled' : 'Disabled'})</button>
        <button onClick={handleChangeFirstEvent}>Update first event name</button>
        <button onClick={handleToggleSettings}>Toggle settings</button>
      </div>
      <CustomComponent tasks={tasks} settingsEnabled={settingsEnabled}/>
    </>
  )
}

export default App;