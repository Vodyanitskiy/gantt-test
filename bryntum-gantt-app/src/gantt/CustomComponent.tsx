import { Column, Grid, Row, TaskModelConfig } from '@bryntum/gantt';
import { BryntumGantt, BryntumGanttProjectModel } from '@bryntum/gantt-react';
import React, { FC, FunctionComponent, useCallback, useMemo, useRef } from 'react';
import { GridColumnConfig } from '@bryntum/gantt/gantt.node';

interface RendererData {
    cellElement: HTMLElement;
    value: unknown;
    record: BryntumGanttProjectModel;
    column: Column;
    grid: Grid;
    row: Row;
    size: {
        height: number;
        configuredHeight: number;
    };
    isExport: boolean;
    isMeasuring: boolean;
}

interface CustomComponentProps {
    tasks: Partial<TaskModelConfig>[];
    settingsEnabled: boolean;
}

export const CustomComponent: FC<CustomComponentProps> = ({ tasks, settingsEnabled }) => {
    const ganttProjectRef = useRef<BryntumGanttProjectModel>();
    const ganttRef = useRef<BryntumGantt>();

    const nameColumnRenderer = useCallback((rendererData: RendererData) => {
        return <NameColumnControl rendererData={rendererData}/>
    }, [])

    const someFieldRenderer = useCallback((rendererData: RendererData) => {
        return <SomeColumnControl rendererData={rendererData} enabled={settingsEnabled}/>
    }, [settingsEnabled]);

    const columns: Partial<GridColumnConfig>[] = useMemo(() => {
        return [
            {
                field: 'name',
                  type: 'name',
                  leafIconCls: '',
                  collapseIconCls: 'b-icon b-fa-caret-down',
                  expandIconCls: 'b-icon b-fa-caret-right',
                  text: 'Title',
                  renderer: nameColumnRenderer,
                  readOnly: true,
                  sortable: false,
            },
            {
                field: 'startDate',
                type: 'date',
                text: 'Start',
                sortable: false,
            },
            {
                field: 'endDate',
                type: 'date',
                text: 'End',
                sortable: false,
            },
            {
                field: 'fullDuration',
                text: 'Duration',
                sortable: false,
                min: 0,
                editor: {
                  min: 0,
                  decimalPrecision: 1,
                },
                step: 1,
                instantUpdate: false,
            },
            {
                field: 'assignees',
                text: 'Some Field',
                sortable: false,
                readOnly: true,
                renderer: someFieldRenderer,
            },
        ]
    }, [nameColumnRenderer, someFieldRenderer])

    return (
      <>
        <BryntumGanttProjectModel
          ref={ganttProjectRef}
          tasks={tasks}
        />

        <BryntumGantt
            ref={ganttRef}
            project={ganttProjectRef}
            config={{
                labelsFeature: {
                    disabled: false,
                    left: {
                        field: "name",
                    },
                }
            }}
            columns={columns}
        />
      </>
  );
}

interface NameColumnControlProps {
    rendererData: RendererData;
}

const NameColumnControl: FunctionComponent<NameColumnControlProps> = ({
  rendererData,
}) => {
  return (
    <div>
        <i>{rendererData?.value ?? ''}</i>
    </div>
  );
};

interface SomeColumnControlProps extends NameColumnControlProps {
    enabled: boolean;
}

const SomeColumnControl: FunctionComponent<SomeColumnControlProps> = ({
  rendererData,
    enabled
}) => {
  return (
    <div>
        {enabled ? rendererData?.value ?? '' : 'Disabled'}
    </div>
  );
};