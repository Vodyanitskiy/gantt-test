import { TaskModelConfig } from '@bryntum/gantt';
import { v4 as uuid } from 'uuid';


export const generateId = (): string => {
  return uuid();
};

export const getRandomDate = (start: Date, end: Date): Date => {
    return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
}

export const getTasks = (tasksNum: number): Partial<TaskModelConfig>[] => {
    const tasks: Partial<TaskModelConfig>[] = [];
    const startDate = new Date("2024-01-01");
    const startEndDate = new Date("2024-02-01");
    const endDate = new Date("2024-03-14");

    for (let i = 0; i <= tasksNum; i++) {
        tasks.push({
            id: generateId(),
            name: `Title-${i}`,
            constraintDate: null,
            constraintType: null,
            draggable: false,
            startDate: getRandomDate(startDate, startEndDate),
            endDate: getRandomDate(startEndDate, endDate),
            manuallyScheduled: false,
            projectConstraintResolution: 'ignore',
            readOnly: false,
            resizable: false,
            timeZone: "UTC",
            eventColor: null,
          })
    }

    return tasks;
}

export const filterTasks = (tasks: Partial<TaskModelConfig>[]): Partial<TaskModelConfig>[] => {
    return tasks.slice(0, 10);
}