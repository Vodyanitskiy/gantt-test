import { TaskModelConfig } from "@bryntum/gantt";
import { Subject, combineLatest, map, startWith, BehaviorSubject } from "rxjs";
import { filterTasks } from './data.ts';

const tasksSubject = new BehaviorSubject<Partial<TaskModelConfig>[]>([]);
const dummyFilterSubject = new Subject<boolean>();
const dummySettingsSubject = new BehaviorSubject<boolean>(false);

export const updateTasks = (tasks: Partial<TaskModelConfig>[]): void => {
    tasksSubject.next(tasks);
}

export const updateFilter = (enabled: boolean): void => {
    dummyFilterSubject.next(enabled);
}

export const toggleSettings = (): void => {
    dummySettingsSubject.next(!dummySettingsSubject.getValue());
}

export const updateFirstEvent = (): void => {
    const tasks = tasksSubject.getValue();
    const updated = {
        ...tasks[0],
        name: `Changed - ${tasks[0].name}`,
    };
    tasks.splice(0, 1, updated);
    tasksSubject.next([ ...tasks ]);
}

export const dummyFilter$ = dummyFilterSubject.asObservable().pipe(startWith(null));

export const dummySettings$ = dummySettingsSubject.asObservable().pipe(startWith(false));

export const tasks$ = combineLatest([
    tasksSubject.asObservable(),
    dummyFilter$,
]).pipe(
    map(([tasks, filterEnabled]) => {
        if (!filterEnabled) return tasks;
        return filterTasks(tasks);
    }),
    startWith([])
);